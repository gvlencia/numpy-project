#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np


# In[2]:


my_list=[1,2,3]
my_list


# In[3]:


np.array(my_list)


# In[5]:


my_matrix=[[1,2,3],[4,5,6],[7,8,9]]
my_matrix


# In[7]:


np.array(my_matrix)


# In[8]:


np.arange(0,10)


# In[9]:


np.arange(0,11,2)


# In[10]:


np.zeros(3)


# In[11]:


np.zeros((5,5))


# In[12]:


np.ones(3)


# In[13]:


np.ones((3,3))


# In[14]:


np.linspace(0,10,3)


# In[15]:


np.linspace(0,10,50)


# In[16]:


np.eye(4)


# In[17]:


np.random.rand(2)


# In[18]:


np.random.rand(5,5)


# In[20]:


np.random.randn(2)


# In[21]:


np.random.randn(5,5)


# In[22]:


np.random.randint(1,100)


# In[24]:


np.random.randint(1,100,10)


# In[25]:


arr=np.arange(25)
ranarr=np.random.randint(0,50,10)


# In[26]:


arr


# In[27]:


ranarr


# In[29]:


arr.reshape(5,5)


# In[30]:


ranarr


# In[31]:


ranarr.max()


# In[32]:


ranarr.argmax()


# In[33]:


ranarr.min()


# In[35]:


ranarr.argmin()


# In[36]:


arr.shape


# In[38]:


arr.reshape(1,25)


# In[39]:


arr.reshape(1,25).shape


# In[40]:


arr.reshape(25,1)


# In[41]:


arr.reshape(25,1).shape


# In[42]:


arr.dtype


# In[43]:


arr=np.arange(0,11)


# In[44]:


arr


# In[45]:


arr[8]


# In[46]:


arr[1:5]


# In[48]:


arr[0:5]


# In[49]:


arr[0:5]=100
arr


# In[51]:


arr=np.arange(0,11)
arr


# In[53]:


slice_of_arr=arr[0:6]
slice_of_arr


# In[54]:


slice_of_arr[:]=99
slice_of_arr


# In[55]:


arr


# In[57]:


arr_copy=arr.copy()
arr_copy


# In[58]:


arr_2d=np.array(([5,10,15],[20,25,30],[35,40,45]))
arr_2d


# In[59]:


arr_2d[1]


# In[60]:


arr_2d[1][0]


# In[61]:


arr_2d[:2,1:]


# In[62]:


arr_2d[2]


# In[63]:


arr_2d[2,:]


# In[65]:


arr=np.arange(1,11)
arr


# In[66]:


arr>4


# In[69]:


bool_arr=arr>4


# In[70]:


bool_arr


# In[71]:


arr[bool_arr]


# In[72]:


arr[arr>2]


# In[73]:


x=2
arr[arr>x]


# In[ ]:




