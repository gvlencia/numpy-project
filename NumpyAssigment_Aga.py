#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np


# In[4]:


np.zeros(10)


# In[6]:


np.ones(10)


# In[10]:


np.ones(10)*5


# In[13]:


np.arange(10,51)


# In[15]:


np.arange(10,51,2)


# In[17]:


arr=np.arange(9)
arr.reshape(3,3)


# In[18]:


np.eye(3)


# In[19]:


np.random.rand(1)


# In[20]:


np.random.randn(25)


# In[29]:


arr=np.linspace(0.01,1.00,100)
arr.reshape(10,10)


# In[30]:


np.linspace(0,1,20)


# In[36]:


mat=np.arange(1,26).reshape(5,5)
mat


# In[38]:


mat[2:,1:]


# In[39]:


mat[3,4]


# In[43]:


mat[:3,1:2]


# In[45]:


mat[4]


# In[47]:


mat[3:,]


# In[49]:


sum(sum(mat))


# In[55]:


mat.std()


# In[50]:


sum(mat)

